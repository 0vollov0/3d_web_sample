export default class {
  private _percent: number;
  private _lastScrllTop: number;
  private _direction: boolean;

  constructor(tagId: string) {
    this._percent = this._lastScrllTop = 0;
    this.setUpScroll(tagId);
    this._direction = true;
  }

  public get percent() : number {
    return this._percent;
  }

  /**
   * down: true
   * up: false
   */
  public get direction() : boolean {
    return this._direction;
  }
  

  scalePercent(start: number, end: number) {
    return (this._percent - start) /(end - start);
  }

  linerInterpolation(p1: number, p2: number, d1: number) {
    return (1-d1)*p1 + d1*p2;
  }

  setUpScroll = (id: string) => {
    document.body.onscroll = () => {
      //calculate the current scroll progress as a percentage
      this._percent =
          ((document.documentElement.scrollTop || document.body.scrollTop) /
              ((document.documentElement.scrollHeight ||
                  document.body.scrollHeight) -
                  document.documentElement.clientHeight)) *
          100
      ;(document.getElementById(id) as HTMLDivElement).innerText =
          'Scroll Progress : ' + this._percent.toFixed(2)

    const st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"

    this._direction = st > this._lastScrllTop;
    
    this._lastScrllTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    }
  }

  
  public get isTop() : boolean {
    return this._percent == 0;
  }
  
  public get isBottom() : boolean {
    return this._percent == 100;
  }
}