import * as THREE from "three";
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import ScrollHandler from './ScrollHandler';
import { AnimationScript } from './types/AnimationScript';
import Levvit from './objects/Levvit'

class Demo {
  private scene: THREE.Scene;
  private camera: THREE.PerspectiveCamera;
  private renderer: THREE.WebGLRenderer;

  private scrollHandler: ScrollHandler;
  clock: THREE.Clock;
  scrollAnimations: AnimationScript[];
  levvitAnimationMixer: THREE.AnimationMixer;
  levvitJump: THREE.AnimationAction;

  private levvit: Levvit;

  private scrollPercent: number;

  constructor() {
    this.scene = new THREE.Scene();
    this.setUpCamera();
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
    });
    this.renderer.setClearColor( 0xBDB0FF, 1);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement);
    window.onresize = this.windowResize.bind(this);

    this.clock = new THREE.Clock();

    this.scrollHandler = new ScrollHandler('scrollProgress');

    // this.turnOnOrbitControls();
    this.turnOnGridHelper(10,5);
    this.setUpLight();
    this.loadAsset();
    this.setUpAnimationScript();
    this.animate();
  }

  turnOnOrbitControls = () => {
    new OrbitControls(this.camera, this.renderer.domElement);
  }

  setUpCamera = () => {
    this.camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 100);
    this.camera.position.y = 0.2;
    this.camera.position.z = 6/this.camera.aspect;
  }

  turnOnGridHelper = (size: number, divisions: number) => {
    const gridHelper = new THREE.GridHelper(size, divisions, 0xaec6cf, 0xaec6cf);
    this.scene.add(gridHelper);
  }

  windowResize = () => {
    window.addEventListener('resize', () => {
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.position.z = 6/this.camera.aspect;
      this.camera.updateProjectionMatrix();
    })
  }

  loadAsset = () => {
    const loader = new GLTFLoader();

    loader.load('asset/Levvit_00.glb', (levvit) => {
      this.levvit = new Levvit(levvit);
      this.levvit.scene.scale.set(2,2,2);
      this.levvit.scene.position.x = -5;
      this.levvit.scene.rotation.y = Math.PI*1/2-0.0001;
      this.scene.add(this.levvit.scene);
    },
      (event: ProgressEvent<EventTarget>) => {
        console.log( ( event.loaded / event.total * 100 ) + '% loaded' );
      },
      (error: ErrorEvent) => {
        console.error(error)
      } 
    );
  }

  setUpLight = () => {
    const light = new THREE.AmbientLight( 0x404040, 5 ); // soft white light
    this.scene.add( light );

    // const pointlight = new THREE.PointLight( 0xff0000, 1, 100 );
    // pointlight.position.set( 0, 10, 0 );
    // this.scene.add( pointlight );

    // const directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
    // this.scene.add( directionalLight );

  }

  animate = () => {
    requestAnimationFrame(this.animate);
    this.renderer.render(this.scene,  this.camera);
    if (this.levvit) {
      this.playScrollAnimations();
      const delta = this.clock.getDelta();
      if (this.levvit) this.levvit.timer(delta);

      if (this.scrollHandler.isTop) {
        this.levvit.turnRight(0.1);
      } else if (this.scrollHandler.isBottom) {
        this.levvit.turnLeft(0.1);
      }
    }
  }

  setUpAnimationScript = () => {
    this.scrollAnimations = new Array<AnimationScript>();
    // this.scrollAnimations.push({
    //   start: 0,
    //   end: 0.1,
    //   func: () => {
    //     if (this.levvit.isRunning) {
    //       this.levvit.stop();
    //     }
    //     if (this.levvit.scene.rotation.y < Math.PI/2) {
    //       this.levvit.scene.rotation.y += 0.1;
    //     }
    //   }
    // })

    this.scrollAnimations.push({
      start: 0,
      end: 100,
      func: () => {
        if (this.scrollHandler.isTop || this.scrollHandler.isBottom) return;
        if (!this.levvit.isRunning) {
          this.levvit.jump();
        }
        if (this.scrollHandler.direction) {
          this.levvit.turnRight(0.1);
        } else {
          this.levvit.turnLeft(0.1);
        }
        this.levvit.scene.position.x = this.scrollHandler.linerInterpolation(-5,5,this.scrollHandler.scalePercent(0,100));
      }
    })

    // this.scrollAnimations.push({
    //   start: 99.9,
    //   end: 100,
    //   func: () => {
    //     if (this.levvit.isRunning) {
    //       this.levvit.stop();
    //     }
    //     if (this.levvit.scene.rotation.y > -Math.PI/2) {
    //       this.levvit.scene.rotation.y -= 0.1;
    //     }
    //   }
    // })
  }

  playScrollAnimations =() => {
    this.scrollAnimations.forEach((animation) => {
      if (this.scrollHandler.percent >= animation.start && this.scrollHandler.percent < animation.end) {
        animation.func();
      }
    })
  }
}

window.onload = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  new Demo();
}