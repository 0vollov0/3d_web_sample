import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import * as THREE from "three";
import { AnimationMixer } from 'three';

export default class {
  private _levvit: GLTF;
  private _mixer: THREE.AnimationMixer;
  private _action_jump: THREE.AnimationAction;

  private flag = false;
  constructor(_levvit: GLTF) {
    this._levvit = _levvit;
    this._setUpAnimation();
  }

  private _setUpAnimation = () => {
    this._mixer = new AnimationMixer(this._levvit.scene);
    this._action_jump = this._mixer.clipAction(this._levvit.animations[0]);
  }

  /**
   * turnRight
   * @param {number} delta - delta of rotation y.
   */
  public turnRight(delta: number) {
    const y = this._levvit.scene.rotation.y;
    // console.log(y);
    if (3/2*Math.PI<= y &&  y < 2*Math.PI) {
      this._levvit.scene.rotation.y += delta;
      if (this._levvit.scene.rotation.y > 2*Math.PI) {
        this._levvit.scene.rotation.y = 0;
      }
    } else if (0 <= y && y < 1/2*Math.PI ) {
      this._levvit.scene.rotation.y += delta;
      if (this._levvit.scene.rotation.y > 1/2*Math.PI) {
        this._levvit.scene.rotation.y = 1/2*Math.PI - 0.0000001;
      }
    } else {
      this._levvit.scene.rotation.y = 3/2*Math.PI;
    }
  }

  /**
   * turnLeft
   * @param {number} delta - delta of rotation y.
   */
  public turnLeft(delta: number) {
    const y = this._levvit.scene.rotation.y;
    if (3/2*Math.PI<= y &&  y < 2*Math.PI) {
      this._levvit.scene.rotation.y -= delta;
      if (this._levvit.scene.rotation.y < 3/2*Math.PI) {
        this._levvit.scene.rotation.y = 3/2*Math.PI;
      }
    }else if (0 < y && y <= 1/2*Math.PI ) {
      this._levvit.scene.rotation.y -= delta;
      if (this._levvit.scene.rotation.y < 0) {
        this._levvit.scene.rotation.y = 2*Math.PI - 0.0000001;
      }
    } else {
      this._levvit.scene.rotation.y = 1/2*Math.PI;
    }
  }

  //** true: right false: left */
  public get direction() : boolean {
    return true;
  }

  public get animations() : THREE.AnimationClip[] {
    return this._levvit.animations;
  }

  public get scene() : THREE.Group {
    return this._levvit.scene;
  }

  public jump = () => {
    this._action_jump.play();
  }

  public stop = () => {
    this._action_jump.fadeOut(1);
  }

  public get isRunning() : boolean {
    return this._action_jump.isRunning();
  }

  public timer = (delta: number) => {
    if (this._mixer) this._mixer.update(delta);
  }
}